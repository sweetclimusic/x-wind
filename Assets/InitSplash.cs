﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class InitSplash : MonoBehaviour {
    [SerializeField]
    private GameObject splashImage;
    [System.NonSerializedAttribute]
    public InitScript sceneStateRef;
    public int signInSceneIndex = 2;
    void Awake() {
        sceneStateRef = Object.FindObjectOfType<InitScript>();
    }

    // Use this for initialization
    void Start () {
        if (!splashImage) {
            splashImage = GameObject.Find("_splashImage");
        }
    }

    IEnumerator DisplaySplash() {
        yield return new WaitForSeconds(3);
        DestroyImmediate(splashImage);
    }
    // Update is called once per frame
    void Update () {
         StartCoroutine("DisplaySplash");
        if (!splashImage) {
            sceneStateRef.activeScene.isLoading = true;
            sceneStateRef.activeScene.loadingProgress = SceneManager.LoadSceneAsync(signInSceneIndex);
        }
    }
}
