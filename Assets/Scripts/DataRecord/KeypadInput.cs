﻿using UnityEngine;
using System;
using System.Globalization;
using System.Collections.Generic;
using UnityEngine.UI;



public class KeypadInput : MonoBehaviour {

    public InputField inputField;
	[System.NonSerializedAttribute]
    public List<int> inputStack;
    public int cap = 7;
    public float timeCap = 3599.999f;
    public int meters = 5;
    [System.NonSerializedAttribute]
    public bool pointEntered = false;
    private int indexOfPoint;
    private float unformattedValue = 0.0f;
    // Use this for initialization
    void Start() {
        inputStack = new List<int>();
        if (!DataSummaryManager.dataloaded) {
            DataSummaryManager.LoadTeamData();
        }
    }
	
	// Update is called once per frame
	void FixedUpdate () {
        string seconds = "";

        inputStack.ForEach(
                (item) => {
                    seconds += item;
                }
        );
        if (pointEntered) {
            seconds = seconds.Insert(indexOfPoint, ".");
        }
        if (inputStack.Count <= cap && seconds != "" ){
        //save unformatted value
        unformattedValue = float.Parse(seconds);
        if(unformattedValue > timeCap) {
                unformattedValue = timeCap;
        }
        
            inputField.text = seconds;
        }

    }
    //only add int to inputstack
	public void updateStack(int item){
        if (inputStack.Count <= cap) { 
            inputStack.Add(item);
        }
    }
    //order 66, if input the values after this is a point
    //track where the user entered a point.
    public void updateStack(string item) {
        if (inputStack.Count <= cap && !pointEntered ) {
            indexOfPoint = inputStack.Count;
            pointEntered = true;
        }
    }

    public void popStack(){
		if(inputStack.Count - 1 >= 0){
			int item = inputStack[inputStack.Count - 1];
			inputStack.Remove(item);
            if(inputStack.Count == 0) {
                inputField.text = "0";
                unformattedValue = 0.0f;
                pointEntered = false;
            }
		}
    }
	public void recordTime(){
        //set the time in seconds to the data object
        DataPoints dp = new DataPoints();
        dp.driverId = SelectedTeam.CurrentTeam.driversLiscense;
        dp.dateTime = DateTime.Now.ToString("dd-MM-yy HM:mm:ss",CultureInfo.InvariantCulture);
        dp.seconds = unformattedValue.ToString("0000.000");
        dp.distance = meters;
        //returns a bool, but not checking
        //if(!inputField.text.Equals("0") && !inputField.text.Equals(""))
        if(inputStack.Count > 0)
        {
            DataSummaryManager.SaveTeamData(dp);
            inputStack.Clear();
            inputField.text = "0";
            pointEntered = false;
            indexOfPoint = 0;
}
    }
}
