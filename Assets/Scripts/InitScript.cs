﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.IO;
using System.Collections;

[System.Serializable]
public class CurrentScene{
	public int currentIndex = 0;
	public bool isLoading = false;
	[System.NonSerializedAttribute]
	public AsyncOperation loadingProgress = null;

}
public class InitScript : MonoBehaviour {

private GameObject app;
public CurrentScene activeScene;

public int firstScene = 1;
	void Awake(){
		app = GameObject.Find("__app");
		DontDestroyOnLoad(app);
       
		activeScene = new CurrentScene();
		activeScene.isLoading = true;
        activeScene.loadingProgress = SceneManager.LoadSceneAsync(firstScene);
	}
    

	public void ExitApplication(){
		#if !UNITY_IOS
			Application.Quit();
		#endif
    }
}

public static class SelectedTeam{
    private static Teams currentSelectedTeam;
	public static Teams CurrentTeam{
		get{
			return currentSelectedTeam;
		}
		set{
            currentSelectedTeam = value;
        }
    }
	///<summary>
	///	Gets the jsonfile, all its contents and writes the CurrentTeam to file
	///</summery>
	public static void SaveTeam(){
		TeamsList teamsList = new TeamsList();
		//Look for existing file and data...
		//COPY PASTE from other script..
		string jsonFilePath = Application.persistentDataPath + "/emotion.json";

		if(!File.Exists(jsonFilePath)){
			//create new and end
			File.Create(jsonFilePath);
            teamsList.teams.Add(currentSelectedTeam);
        }else{
			//load objects into raceTeamList from json.
			string jsonFileAsString = File.ReadAllText(jsonFilePath);
			//populate object with json data
			JsonUtility.FromJsonOverwrite(jsonFileAsString,teamsList);
			if(teamsList != null){
                //Loop and find are teamster
                int index = -1;
                foreach (var item in teamsList.teams)
				{
					if(item.driversLiscense == currentSelectedTeam.driversLiscense){
                        index = teamsList.teams.IndexOf(item);
                        continue;
                    }
				}
				if(index > -1){
                    teamsList.teams[index] = currentSelectedTeam;
                }
			}else{
				teamsList.teams.Add(currentSelectedTeam);
			}
		}
		//write to JSON file with currentSelectedTeam and any changes

        //save this to json
		string json = JsonUtility.ToJson(teamsList);
		File.WriteAllText(jsonFilePath,json);
	}
}