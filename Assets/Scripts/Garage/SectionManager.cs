﻿using UnityEngine;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

//Manage the section visiblity
public class SectionManager : MonoBehaviour
{
    //holds the GameObject indicatiors found in the progress navpanel so we can fire their toggle image script
    //which indexs well be back, current, next. But button navication and section selection
    public List<int> sceneIndexes;
    public List<ImageToggle> progress;
    public string firstCarPartSceneName = "Chassi";
    public int sceneSelectionOffset = 5;
    //first car part scene number

    public Button previousButton;
    public Button nextButton;

    [SerializeField]
    private int previousSceneIndex;
    public Text previousButtonText;
    [SerializeField]
    private int nextSceneIndex;
    [SerializeField]
    private bool[] valuesForSaving;
    public Text nextButtonText;
    
    [System.NonSerializedAttribute]
    public SectionDetails currentSectionDetails;
    [System.NonSerializedAttribute]
    public int currentSection;
    public bool cpOn;
    public bool setToogleGroup;
    public ToggleGroup tgrouping;
    public Toggle topToggle;
    public Transform leftToogleLayoutGroup;
    //sets the control panel on or off
    public CanvasGroup cpGroup;
    public Text instructionPanelText;
    public Image instructionPanelImage;
    public Image leftTextGroupImage;
    [HideInInspector]
    public List<Toggle> availableToogles;
    public GameObject summaryPanel;
    //for going back to main menu
    [System.NonSerializedAttribute]
    public InitScript sceneStateRef;
    public int mainMenuSceneIndex = 2;
    void Awake ()
    {
        // if(currentSectionDetails.Equals(null)){
        // 	currentSectionDetails = new SectionDetails();
        // }
        //list of all CAR part scenes in a dictionary, ScetionDetails know their neighbouring scene.
        for (int i = sceneSelectionOffset; i < SceneManager.sceneCountInBuildSettings; i++) {
            sceneIndexes.Add (i);

        }
        availableToogles = new List<Toggle>();
        sceneStateRef = Object.FindObjectOfType<InitScript>();
    }

    void Start ()
    {
        //wanting to be the main active scene
        SceneManager.SetActiveScene (SceneManager.GetSceneByName ("Garage")); 

        //there should be our first SectionDetails
        currentSectionDetails = GameObject.FindGameObjectWithTag ("details").GetComponentInChildren<SectionDetails> ();
        UpdateSceneSelectionValue (new Dictionary<int, string> () {
            { currentSectionDetails.previousSceneIndex,currentSectionDetails.previousSceneName },
            { currentSectionDetails.nextSceneIndex, currentSectionDetails.nextSceneName }
        }
        );
        currentSection = currentSectionDetails.currentSceneIndex;
        setToogleGroup = currentSectionDetails.useToogleGrouping;
        HideControlPanel (currentSectionDetails.useControlPanel);
        valuesForSaving = new bool[currentSectionDetails.selections.Length];

        
        UpdateSectionSelection();
        LoadDisplayDefaults();
        //but how to get this details next scene load?
    }

    ///<summary>
    /// Load details from the selected scene and select the defaults to display on the panel
    ///</summary>
    public void CurrentLoadDetails (SectionDetails detailsObject)
    {
        currentSectionDetails = detailsObject;
        setToogleGroup = detailsObject.useToogleGrouping;
        valuesForSaving = new bool[currentSectionDetails.selections.Length];
        //empty out incase there is a scene not using the panel.
        availableToogles.Clear();

        UpdateControlPanelToogle();
        UpdateSectionSelection();
        LoadDisplayDefaults();
        /*
           //broadcast to the sectionManager on the Garage scene
               HideControlPanel(controlPanelDetails.useControlPanel);
			   BindToggleToGroup(currentSectionDetails.useToogleGrouping)
               UpdateControlPanelToggle();
               UpdateSectionSelection());
       */
    }

    ///<summary>
    /// For the current scene, use the scene details and the saved data for selected teams to display
    /// user changes.
    ///</summary>
    public void LoadDisplayDefaults()
    {
        string currentCarPartName = CurrentSectionName();
        Teams ccmodel = SelectedTeam.CurrentTeam;
        if (availableToogles.Count != 0) {
            int refIndex = -1;
            //start setting up defaults...
            switch (currentCarPartName)
            {
                case "Chassi":
                    if (availableToogles.Count == 5)
                    {
                        //as we expect values... start to set them
                        //expecting them to allwas be in order.
                        valuesForSaving[0] = availableToogles[0].isOn = ccmodel.posOne;
                        valuesForSaving[1] = availableToogles[1].isOn = ccmodel.posTwo;
                        valuesForSaving[2] = availableToogles[2].isOn = ccmodel.posThree;
                        valuesForSaving[3] = availableToogles[3].isOn = ccmodel.posFour;
                        valuesForSaving[4] = availableToogles[4].isOn = ccmodel.posFive;
                        //So scrub this coding .... deadlines 
                    }
                    break;
                //these are all in three and must have a grouping
                case "FrontSpindal":
                    refIndex = ccmodel.spindalPosition;
                    break;
                case "RearAxel":
                    refIndex = ccmodel.rearAxelPosition;
                    break;
                case "Wheel":
                    refIndex = ccmodel.wheelType;
                    break;
                case "RubberBand":
                    refIndex = ccmodel.bandType;
                    break;
            }
            if(refIndex > -1){
                int index = -1;
                foreach (var item in availableToogles)
                {
                    index = availableToogles.IndexOf(item);
                    if(index != refIndex){
                        availableToogles[index].isOn = false;
                    }
                }
                availableToogles[refIndex].isOn = true;
            }
            //using a switch went through the scene selecting user options from JSON.

        }
        //no toggle group,
         if(currentCarPartName == "Summary"){
            summaryPanel.SetActive(true);
        }
    }

    public string CurrentSectionName() {
        //this should work on how the editor is loading the scenes
        string currentCarPartName = SceneManager.GetSceneAt (0).name;
        if(currentCarPartName == "Garage"){
            //loop to find none Garage scene
            for (int n = 0; n < SceneManager.sceneCount; ++n)
            {
                Scene scene = SceneManager.GetSceneAt(n);
                if(scene.isLoaded && scene.name != "Garage"){
                    return SceneManager.GetSceneAt(n).name;
                }
            }
        }
        return currentCarPartName;
    }

    public void UpdateSectionSelection ()
    {
        //get selected and toggle the rest should be off.
       progress [(sceneIndexes [currentSection] - sceneSelectionOffset)].Toggle();
    }

    //determines if the controlpanel is need with the loaded scene
    public void HideControlPanel (bool pOff = false)
    {
        cpOn = pOff;
        cpGroup.alpha = (cpOn ? 1.0f : 0.0f);
        UpdateControlPanelToogle ();
    }

	
    ///<summary>
    /// update the toggle control panel with all the values from the sectionDetails
    ///</summary>
    public void UpdateControlPanelToogle ()
    {
       
            Color tempColor = currentSectionDetails.panelColors;
            leftTextGroupImage.color = new Color(tempColor.r,
                                                tempColor.g,
                                                tempColor.b,
                                                currentSectionDetails.alphaValue);
            instructionPanelImage.color = currentSectionDetails.panelColors;
            instructionPanelText.text = currentSectionDetails.Instructions.text;
        //if not hiding, populate with options and initial value.
        if (cpOn)
        {
            for (int i = 0; i < (int)currentSectionDetails.selections.Length; i++) {
                int tempValue = i; //have to use a temp value, doesn't remener the i on delegate/lambda click
                if (i == 0) {
                    topToggle.GetComponentInChildren<Text> ().text
					 = currentSectionDetails.selections [i];
                    if (setToogleGroup) {
                        topToggle.group = tgrouping;
                    }
                    //save the value anytime a toggle changes
                    topToggle.onValueChanged.AddListener (
                        delegate {
                            if (topToggle.isOn) {
                                valuesForSaving [tempValue] = topToggle.isOn;
                            }
                        }
                    );
                    availableToogles.Add(topToggle);
                } else {
                    //else we need to create new text objects
                    GameObject nextToggleObject = (GameObject)Instantiate (topToggle.gameObject);
                    nextToggleObject.name = currentSectionDetails.selections [i] + "Object";
                    Text childText = nextToggleObject.GetComponentInChildren<Text> ();
                    
                    childText.text = currentSectionDetails.selections [i];
                    if (setToogleGroup) {
                        topToggle.group = tgrouping;
                    }
                    Toggle tempToggle = nextToggleObject.GetComponent<Toggle> ();
                    //save the value anytime a toggle changes
                    tempToggle.onValueChanged.AddListener (
                        delegate {
                            if (tempToggle.isOn) {
                                valuesForSaving [tempValue] = tempToggle.isOn;
                            }
                        }
                    );
                    //place to put the button in the scroll view list
                    nextToggleObject.transform.SetParent (leftToogleLayoutGroup, false);
                    availableToogles.Add(tempToggle);
                }
            }
			//add the event listener to all toogles in availableToogles
            //save the value anytime a toggle changes
                    // tempToggle.onValueChanged.AddListener (
                    //     delegate {
                    //         if (tempToggle.isOn) {
                    //             valuesForSaving [tempValue] = tempToggle.isOn;
                    //         }
                    //     }
                    // );
        }
    }

	

    //hopefully to remain unused as delegate onValueChanged does this none loop.
    public void UpdateControlToggleValue ()
    {
        //from the parent leftToogleLayoutGroup
        Toggle[] tvalues = leftToogleLayoutGroup.GetComponentsInChildren<Toggle> ();
        //store ones that are on
        int key = 0;
        foreach (var item in tvalues) {
            if (item.isOn) {
                valuesForSaving [key] = item.isOn;
            }
            key++;
        }
    }

    /** 
	* UpdateSceneSelectionValue
	* param name="args" expecte string array length 2 to have the previousScene and nextScene name from the Gameobject with SectionDetails.cs
	* 
	*/
    public void UpdateSceneSelectionValue (Dictionary <int,string> args)
    {
        //use the sceneIndex for loading
        int[] key = new int[args.Keys.Count];
        args.Keys.CopyTo (key, 0);
        string[] values = new string[args.Keys.Count];
        args.Values.CopyTo (values, 0);
        previousSceneIndex = sceneIndexes [key [0]];
        nextSceneIndex = sceneIndexes [key [1]];
        previousButton.onClick.AddListener (delegate {
            LoadScene (previousSceneIndex);
        });
        previousButtonText.text = values [0];
        //update next button click and text
        nextButton.onClick.AddListener (delegate {
            LoadScene (nextSceneIndex);
        });
        nextButtonText.text = values [1];
		
    }

    public void LoadScene (int sceneIndex)
    {
        //turn off Light
        UpdateSectionSelection();
        //SceneLoadClick must be attached to parent and Loadingscreen/LoadingUpdate script
        gameObject.BroadcastMessage ("LoadCarSceneByIndexAddative", sceneIndex);
    }

    public void SaveCarModel ()
    {
        UpdateTeamObject ();
        SelectedTeam.SaveTeam();
    }

    public void ExitApplication ()
    {
        #if !UNITY_IOS
        Application.Quit ();
        #endif
    }
    public void Back() { sceneStateRef.activeScene.isLoading = true;
        sceneStateRef.activeScene.loadingProgress = SceneManager.LoadSceneAsync(mainMenuSceneIndex);
    }

    public void UpdateTeamObject ()
    {
        string currentCarPartName = CurrentSectionName();

        Teams ccmodel = SelectedTeam.CurrentTeam;
        int result;
        switch (currentCarPartName) {
        case "Chassi":
            ccmodel.posOne = valuesForSaving [0];
            ccmodel.posTwo = valuesForSaving [1];
            ccmodel.posThree = valuesForSaving [2];
            ccmodel.posFour = valuesForSaving [3];
            ccmodel.posFive = valuesForSaving [4];
            break;
        case "FrontSpindal":
            result = FindTruthInValuesForSaving ();
            if (result > -1) {
                ccmodel.spindalPosition = result;
            }
            break;
        case "RearAxel":
            result = FindTruthInValuesForSaving ();
            if (result > -1) {
                ccmodel.rearAxelPosition = result;
            }
            break;
        case "Wheel":
            result = FindTruthInValuesForSaving ();
            if (result > -1) {
                ccmodel.wheelType = result;
            }
            break;
        case "RubberBand":
            result = FindTruthInValuesForSaving ();
            if (result > -1) {
                ccmodel.bandType = result;
            }
            break;
        }
        //using a switch went through the scene selecting carparts ready to save to JSON.
        //rewrite to static object.
        SelectedTeam.CurrentTeam = ccmodel;
    }


    private int FindTruthInValuesForSaving ()
    {
        for (int i = 0; i < valuesForSaving.Length; i++) {
            // should be in a toogle group so only one value is true;
            if (valuesForSaving [i]) {
                return i;
            }
        }
        return -1;
    }
    //UNUSED stuff, but don't what to lose
    //grabs the values that the user change for the current scene, displays it on the control panel
    // public void UpdateControlPanelText(){
    // 	//if not hiding, populate with options and initial value.
    // 	if(cpOn){
			
    // 		for (int i = 0; i < (int)currentSectionDetails.selections.Length; i++)
    // 		{
    // 			int tempValue = i; //have to use a temp value, doesn't remener the i on delegate/lambda click
    // 			if(i==0){
    //                 topText.text = currentSectionDetails.selections[i];
                    
    //                 topText.GetComponent<Button>().onClick.AddListener( () =>
    // 					UpdateSliderValue(tempValue)
    //                 );
    //             }
    // 			else{
    //                 //else we need to create new text objects
    // 				GameObject nextTextObject = (GameObject)Instantiate(topText.gameObject);
    //                 nextTextObject.name = currentSectionDetails.selections[i] + "Object";
    //                 Text childText = nextTextObject.GetComponentInChildren<Text>();
    //                 childText.text =  currentSectionDetails.selections[i];

    // 				if(i != (int)currentSectionDetails.selections.Length - 1 ){
    // 					childText.alignment = TextAnchor.MiddleCenter;
    // 				}else{
    // 					childText.alignment = TextAnchor.LowerCenter;
    // 				}
                    
    //                 childText.GetComponentInChildren<Button>().onClick.AddListener( () =>
    //                 	UpdateSliderValue(tempValue)
    //                 );
    // 				//place to put the button in the scroll view list
    // 				nextTextObject.transform.SetParent(leftLayoutGroup,false);
    //             }
    //         }
    // 		//
    //         options.maxValue = (int)currentSectionDetails.selections.Length -1;
    //         options.value = currentSectionDetails.defaultValue;
    // 		//use a delegate to bind function on change in script... could be done in editor blah blah
    // 		options.onValueChanged.AddListener ( delegate{UpdateControlPanelValue();});
    // 	}
    // }
    // public void UpdateControlPanelValue(){
    // 	currentSectionDetails.defaultValue = (int)options.value;
    // }
    /** UpdateSliderValue
	*  @param name="newValue" int value for the slider
	*  internal helper function.
	*/
    // public void UpdateSliderValue(int newValue){
    //     options.value = newValue;
    // }

    
}
