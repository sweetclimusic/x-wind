﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
public class SectionDetails : MonoBehaviour {
    public string[] selections;
	public Text Instructions;
	public bool useControlPanel;
    public Color panelColors;
	public string previousSceneName;
	public string nextSceneName;
	public int previousSceneIndex;
	public int nextSceneIndex;
    public int currentSceneIndex;
    public bool useToogleGrouping;
    public float alphaValue = 0.7f;

	/*
		I don't want the GARAGE scene reloading all the time.... but it is! so need to rework this stuff
		and the CurrentLoadDetails needs to come into play.
	*/
    void Awake(){
		string activeSceneName = SceneManager.GetActiveScene().name;
		//if this scene is loaded but no Garage, load that.
		Scene mainUINeeded = SceneManager.GetSceneByName("Garage");
		if(!mainUINeeded.isLoaded){
            StartCoroutine(LoadScene(activeSceneName));
		}
		
	}
	IEnumerator LoadScene(string activeSceneName){
		AsyncOperation loadScene =  SceneManager.LoadSceneAsync("Garage",LoadSceneMode.Additive);
        yield return loadScene;
		if(loadScene.isDone){
            //Debug.Log(GameObject.FindGameObjectWithTag("manager").GetComponentInChildren<SectionManager>());

            //garageManagerObject.CurrentLoadDetails(this);
            //might need to send a gameobject to the other scene.
            gameObject.name = activeSceneName;
            SceneManager.MoveGameObjectToScene(gameObject, SceneManager.GetSceneByName("Garage"));
        }
	}
}
