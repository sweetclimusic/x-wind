﻿using UnityEngine;
using UnityEngine.UI;

public class GatherSummary : MonoBehaviour {

    public Text ChassisText;
	public Text RearAxelText;
	public Text FrontSpindalText;
	public Text ElasticText;
	public Text WheelText;

    // Use this for initialization
    void OnEnable() {
		//zero base numbering so + 1
        ElasticText.text += " " + (SelectedTeam.CurrentTeam.bandType+1);
		RearAxelText.text += " " + (SelectedTeam.CurrentTeam.rearAxelPosition+1);
		FrontSpindalText.text += " " + SelectedTeam.CurrentTeam.spindalPosition;
		WheelText.text += " " + SelectedTeam.CurrentTeam.wheelType;
        string mods = " " + ((SelectedTeam.CurrentTeam.posOne ? "" : "1,") + (SelectedTeam.CurrentTeam.posTwo ? "" : "2,") +
        (SelectedTeam.CurrentTeam.posThree ? "" : "3,") + (SelectedTeam.CurrentTeam.posFour ? "" : "4,") + (SelectedTeam.CurrentTeam.posFive ? "" : "5"));
        
        //trim trailing comma
        ChassisText.text += mods.TrimEnd(',');
    }
}
