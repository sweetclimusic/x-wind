﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using System.Linq;  //used for nice data manipulation
using System;

delegate float Normalizer(int i,float f, float n);
// create a bar view port that has a L chart sliced sprite as its background.
// create bar holder. 
// 	create a text below bar holder for text of the X value (static text)
// 	create 3 text along bar Y , Min, MID, Max values
// 	create a bar,
// 		create a barValue that is at the top of a bar. it contains the value
// 		change the pivot point to bottom left so it rendered from sw to ne

public class BarChart : MonoBehaviour {

    //reference to the gameobject that is a bar chart instance
    public Bar barPrefab;
    public float timeCap = 3599.999f;
    //ints for graphs
    [HideInInspector]
    public int[] inputValues;

    public points[] labels;
    public struct points {
        public string inputTimes;
        public float inputValue;
        public string sprintDistance;
    }

    //restrict to parent container and not screen size... 
    //hopefully parent container is in screen size
    public RectTransform parentContainer;
    public RectTransform barChartContiner;


    //using the games pallet, give the generated bars colors
    public Color[] colorPallet;
    public float alpha = 0.7f;

    //[HideInInspector]
    //public Text maxText;
    //[HideInInspector]
    //public Text avgText;
    //[HideInInspector]
    //public Text minText;

    // variable to normalize the chart size based on max height value
    float chartHeight = 0.0f;
    Normalizer normalizer;
    void Start() {
        if (!DataSummaryManager.dataloaded) {
            StartCoroutine(LoadChartData());
        }
        //continue if data to use.
        if (DataSummaryManager.dataloaded) {
            chartHeight = Screen.height + barChartContiner.sizeDelta.y;
            chartHeight = parentContainer.sizeDelta.y + barChartContiner.sizeDelta.y;
            //delegates, using these as will be recalling the formats multiple times.
            normalizer = (i,f, n) => { return ((float)i / f) * n; };
            
            Loadpoints();
            DisplayGraph(inputValues);
        }  

    }

    IEnumerator LoadChartData() {
        //load chartData
        while (!DataSummaryManager.LoadTeamData()) {
            yield return null;
        }

    }

    ///<summary>
    ///displaying a graph `rounds` to int to lower any complexity that may happen with float values.
    ///</summary>
    public void DisplayGraph(int[] vals) {
        float maxValue = vals.Max();
        //cap maxValue to 59:59.999 mins,sec,millisec... unrealistic but
        int colorIndex = 0;
        for (int i = 0; i < vals.Length; i++) {

            Bar newBar = Instantiate(barPrefab) as Bar;
            //assumes this is attached to it's own object.
            newBar.transform.SetParent(transform);
            // newBar.bar, 'bar' is an Image
            Image im = newBar.bar;
            RectTransform rt = im.GetComponent<RectTransform>();

            //color our bar
            //find the remainding value, gets cast to int, so floors to 0 being a 'loop'
            colorIndex = i % colorPallet.Length;
            im.color = new Color(colorPallet[colorIndex].r
                , colorPallet[colorIndex].g
                , colorPallet[colorIndex].b
                , alpha
                );

            //normalize the value of this chart to never be draw to real size
            //but to restricted size
            //(0.95f is an offset to prevent the graph goeing to long)...
            //offset is incorrect need a different one.
            
            rt.sizeDelta = new Vector2(rt.sizeDelta.x, chartHeight * normalizer(vals[i],maxValue,0.35f));

            //bonus stuff for text values.
            newBar.barLabel.text = labels[i].inputTimes; //("MM-dd-yy H:mm:ss");
            newBar.barValue.text = labels[i].inputValue.ToString();

            //test the value and move the pivot on the text for small values on height,
            //watch vid 4 for the pivot thing.
            if (rt.sizeDelta.y < 30.0f) {
                RectTransform rtBarValue = newBar.barValue.GetComponent<RectTransform>();
                rtBarValue.pivot = new Vector2(0.5f, 0f);
                rtBarValue.anchoredPosition = Vector2.zero;
                rt.sizeDelta = new Vector2(rt.sizeDelta.x, chartHeight * normalizer(vals[i], maxValue, 1.96f));

            }
        }
    }
    /// <summary>
    /// 
    /// </summary>
    public void Loadpoints() {
        var dl = DataSummaryManager.dataSerialized.dataPoints;
        //we should know how many data point the driver has.
        //these array set to there size..
        inputValues = new int[dl.Count];
        labels = new points[dl.Count];
        points tempPoint = new points();
        foreach (var item in dl) {
            float sec = float.Parse(item.seconds);
            //flatten float value to int, using the string representation
            if (sec > timeCap) {
                sec = timeCap;
            }
            inputValues[dl.IndexOf(item)] = (Int32)sec;

            tempPoint.inputTimes = item.dateTime;
            tempPoint.inputValue = sec;
            tempPoint.sprintDistance = item.distance.ToString();
            labels[dl.IndexOf(item)] = tempPoint;
        }
    }

}
