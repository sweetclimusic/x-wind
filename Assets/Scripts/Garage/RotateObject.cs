﻿using UnityEngine;
using System.Collections;

public class RotateObject : MonoBehaviour {

    public float rotateSpeed = 0.3f;
    public GameObject rotObject;
    [Range(0,1)]
    public int inWorld = 0;
	
	// Update is called once per frame
	void Update () {
		rotObject.transform.Rotate(0,rotateSpeed*Time.deltaTime,0,(Space)inWorld);
	}
}

