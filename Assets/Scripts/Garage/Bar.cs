﻿using UnityEngine;
using UnityEngine.UI;

public class Bar : MonoBehaviour
    {
        public Image bar;
        public Text barLabel; // label for each bar, will be the time stamp
        public Text barValue;
    }