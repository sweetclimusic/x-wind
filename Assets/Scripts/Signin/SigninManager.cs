﻿using UnityEngine;
using UnityEngine.SceneManagement;
public class SigninManager : MonoBehaviour {

	// Use this for initialization
	public GameObject loadingScreen;
	[System.NonSerializedAttribute]
	public InitScript sceneStateRef;
	public int mainMenuSceneIndex = 2;
	GameObject appLoadScreen;
	void Awake(){
		sceneStateRef = Object.FindObjectOfType<InitScript>();
		appLoadScreen = GameObject.Find("LoadingScreen");
		DontDestroyOnLoad(appLoadScreen);
	}
	void Start(){

		loadingScreen.SetActive(true);
	}
	public void LoadNextScene(){
		sceneStateRef.activeScene.isLoading = true;
		sceneStateRef.activeScene.loadingProgress = SceneManager.LoadSceneAsync(mainMenuSceneIndex);
	}
	/*
		IEnumerator LoadNextScene(){
		sceneStateRef.activeScene.isLoading = true;
		sceneStateRef.activeScene.loadingProgress = SceneManager.LoadSceneAsync(mainMenuSceneIndex);
		//send selected Team Gameobject to __app scene for the Garage to use.
		yield return sceneStateRef.activeScene.loadingProgress;
		if(sceneStateRef.activeScene.loadingProgress.isDone){
            
            //garageManagerObject.CurrentLoadDetails(this);
            gameObject.name = activeSceneName;
            SceneManager.MoveGameObjectToScene(gameObject, SceneManager.GetSceneByName("Garage"));
        }
	}
	*/
}
