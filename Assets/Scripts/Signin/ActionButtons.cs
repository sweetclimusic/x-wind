﻿using UnityEngine;
using System.IO;
using UnityEngine.UI;
using System;
using UnityEngine.SceneManagement;
public class ActionButtons : MonoBehaviour {

	public InputField[] inputs;
	[System.NonSerializedAttribute]
	public PopulateUserList userListScript;
	
	public GameObject loadingImage;
	// Use this for initialization
	void Start(){
		userListScript = gameObject.GetComponentInChildren<PopulateUserList>();
	}
	public void Save(){
		//public string teamName;
		//public string driverName1;
		//public string driverName2;
		//public string driversLiscense;
		Teams newTeam = new Teams();
		newTeam.driversLiscense = Guid.NewGuid().ToString();
		//adding the 3 InputField to an array
		newTeam.teamName =inputs[0].text;
		newTeam.driverName1 =inputs[1].text;
		newTeam.driverName2 =inputs[2].text;
		userListScript.teamsList.teams.Add(newTeam);
		//save this to json
		string json = JsonUtility.ToJson(userListScript.teamsList);
		string jsonFilePath = Application.persistentDataPath + "/emotion.json";

		if(!File.Exists(jsonFilePath)){
			//create new and end
			File.Create(jsonFilePath);
		}
		File.WriteAllText(jsonFilePath,json);
		//add that team to the static variable
        SelectedTeam.CurrentTeam = newTeam;
        //move to next scene start loading process
        loadingImage.SetActive(true);
		SceneManager.LoadSceneAsync(2);

		
	}
	public void Clear(){
		foreach (var item in inputs)
		{
			item.text = "";
		}
	}
}
