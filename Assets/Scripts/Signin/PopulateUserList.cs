﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;
using System.IO;
using UnityEngine.EventSystems;

/**
* PGC for the icons that appear in the RaceTeamButton TeamIcon
*/
[System.Serializable]
public class IconParty{
	public List<Sprite> avalibleIcons;
	public Sprite generateIcon(){
		return avalibleIcons[Random.Range(0,avalibleIcons.Count)];
	}
}

public class PopulateUserList : MonoBehaviour {
	public GameObject driverButton;
    private Dictionary<string, GameObject> generatedButtons;
    public TeamsList teamsList;
	public IconParty teamIconList;
	public Transform contentArea;
	[System.NonSerializedAttribute]
	public SigninManager signinScriptObject;
	public void PopulateList(){
		
		foreach (var item in teamsList.teams)
		{
			GameObject currentRaceTeamBtn = Instantiate (driverButton) as GameObject;
			RaceTeamButton raceTeamBtn = currentRaceTeamBtn.GetComponent<RaceTeamButton> ();
			raceTeamBtn.teamIcon.sprite = teamIconList.generateIcon();
			raceTeamBtn.teamName.text = item.teamName;
			raceTeamBtn.mainDriver.text = item.driverName1;
			raceTeamBtn.secondDriver.text = item.driverName2;
			raceTeamBtn.LicenseId = item.driversLiscense;
			raceTeamBtn.name = item.driversLiscense;
			//temp CarModel struct
			raceTeamBtn.CurrentTeam = item;
			// currentCar.spindalPosition = item.spindalPosition;
            // currentCar.rearAxelPosition = item.rearAxelPosition;
            // currentCar.color = item.color;
			// currentCar.wheelType = item.wheelType;
			// currentCar.bandType = item.bandType;
			// currentCar.chassisModified = item.chassisModified;
			// if(currentCar.chassisModified){
			// 	//chassisModifications
			// 	currentCar.posOne = item.posOne;
			// 	currentCar.posTwo = item.posTwo;
			// 	currentCar.posThree = item.posThree;
			// 	currentCar.posFour = item.posFour;
			// 	currentCar.posFive = item.posFive;
			// }
			// raceTeamBtn.CurrentCarModel = currentCar;
			
			raceTeamBtn.raceTeamButton.onClick.AddListener(delegate { selectTeam(); });
			var tempName = item.driversLiscense;

            Button[] btns = currentRaceTeamBtn.GetComponentsInChildren<Button>();
			foreach (var btn in btns)
			{
				if(btn.name == "RemoveDriver"){
					Button tempBtn = (Button)Instantiate(raceTeamBtn.removeRaceTeam);
                    
                    tempBtn.onClick.AddListener(delegate { removeTeam(tempName); });
                    tempBtn.transform.SetParent(raceTeamBtn.removeRaceTeam.transform,false);
                }

			}
			//place to put the button in the scroll view list
			raceTeamBtn.transform.SetParent(contentArea,false);
            generatedButtons.Add(tempName, currentRaceTeamBtn);
        }
	}
	//TODO
			/*
			add button click function to delete driver
			add button click function to log driver in
			*/  
	//remove a player
	
	//login a player
	public void selectTeam() {
        //try to get the button click event!
        GameObject srButton = EventSystem.current.currentSelectedGameObject;
        RaceTeamButton selectedRaceTeamBtn = srButton.GetComponent<Button>().GetComponentInChildren<RaceTeamButton>();
        //hopefully goes next to __app
		SelectedTeam.CurrentTeam = selectedRaceTeamBtn.CurrentTeam;
        DataSummaryManager.dataloaded = false;

        //make a new team and set the stuff that RaceTeamButton has on it.
        //selectedTeam = selectedRaceTeamBtn.CurrentTeam;
        //selectedTeam.teamIcon =  selectedRaceTeamBtn.teamIcon.sprite;
        // selectedTeam.teamName = selectedRaceTeamBtn.teamName.text;
		// selectedTeam.driverName1 = selectedRaceTeamBtn.mainDriver.text;
		// selectedTeam.driverName2 = selectedRaceTeamBtn.secondDriver.text;
		// selectedTeam.driversLiscense = selectedRaceTeamBtn.LicenseId;
		// signinScriptObject.currentSelectedTeam = selectedTeam;
		signinScriptObject.LoadNextScene();
	}

	public void removeTeam(string currentTeam){
		//from remove team get the parent raceTeamBtn and then LicenseId
		bool found = false;
        //find this team  with the license and remove it from the list
        var index = -1;
        foreach( var item in teamsList.teams ){
			if(item.driversLiscense== currentTeam){
                index = teamsList.teams.IndexOf(item);
				found = true;
				continue;
			}
		}
		if(found){
            teamsList.teams.RemoveAt(index);
            var clone = generatedButtons[currentTeam];
            Destroy(clone,0.5f);
            generatedButtons.Remove(currentTeam);
            //save this change to the json file
            string json = JsonUtility.ToJson(teamsList);
			string jsonFilePath = Application.persistentDataPath + "/emotion.json";

			if(!File.Exists(jsonFilePath)){
				//create new and end
				File.Create(jsonFilePath);
			}
			File.WriteAllText(jsonFilePath,json);
		}
		
	}
	void Awake(){
        generatedButtons = new Dictionary<string, GameObject>();
        if(LoadUsersFromFile()){
			PopulateList();
		}
		signinScriptObject = gameObject.GetComponent<SigninManager>();
	}

	public bool LoadUsersFromFile(){
		string jsonFilePath = Application.persistentDataPath + "/emotion.json";

		if(!File.Exists(jsonFilePath)){
			//create new and end
			File.Create(jsonFilePath);
			return false;
		}
	
		//load objects into raceTeamList from json.
		string jsonFileAsString = File.ReadAllText(jsonFilePath);
		
		//populate object with json data
		JsonUtility.FromJsonOverwrite(jsonFileAsString,teamsList);
		
		if(teamsList == null){
			return false;
		}
		return true;
	}

}
