﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ImageToggle : MonoBehaviour
{

    public bool toggleImage;
    public Sprite onImage;
    public Sprite offImage;
    public Image imageRef;

    public void Start(){
        imageRef.sprite = offImage;
    }
    public void Update(){
        StartCoroutine(ToggleActive());
    }
    public void Toggle(){
        toggleImage = !toggleImage;
    }
    IEnumerator ToggleActive (){
        imageRef.sprite = (toggleImage ? onImage : offImage);
        yield return null;
    }
}
