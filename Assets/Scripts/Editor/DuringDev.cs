﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEditor;

public class DuringDev : Editor {
public bool sceneOverride = false;
	public string sceneName = "";
	// Use this for initialization
  void Awake()
  {
  	GameObject check = GameObject.Find("__app");
  	if (check==null && sceneOverride == false){
		   UnityEngine.SceneManagement.SceneManager.LoadScene("Preload"); 
		}
		// if(check == null && sceneOverride){
		// 	UnityEngine.SceneManagement.SceneManager.LoadScene(sceneName);
		// }
  }
	// adds a menu item which gives a brief summary of currently open scenes
    [MenuItem("SceneExample/Scene Summary")]
    public static void ListSceneNames()
    {
        string output = "";
        if (SceneManager.sceneCount > 0)
        {
            for (int n = 0; n < SceneManager.sceneCount; ++n)
            {
                Scene scene = SceneManager.GetSceneAt(n);
                output += scene.name;
                output += scene.isLoaded ? " (Loaded, " : " (Not Loaded, ";
                output += scene.isDirty ? "Dirty, " : "Clean, ";
                output += " Index: " + scene.buildIndex + ",";
                output += scene.buildIndex >=0 ? " in build)\n" : " NOT in build)\n";
                
            }
        }
        else
        {
            output = "No open scenes.";
        }
        EditorUtility.DisplayDialog("Scene Summary",output, "Ok");
    }
}
