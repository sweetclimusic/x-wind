﻿using UnityEngine;
using UnityEngine.UI;

public class ToggleText : MonoBehaviour {

	public string switchText = "";
	public GameObject objectWithText;
	private Text childObject;
	private string baseText;
	void Start(){
		childObject = objectWithText.GetComponentInChildren<Text>();
		baseText = childObject.text;
	}
	public void toggleText(){
		
		if(childObject.text == baseText){
			childObject.text = switchText;
		}else
		{
			childObject.text = baseText;
		}
	}

}
