﻿using UnityEngine;

//This class is used to keep the details of the player selection scene by scene.

public class SceneLoadClick: MonoBehaviour {

	public GameObject loadingScreen;
	[System.NonSerializedAttribute]
	public InitScript sceneStateRef;
	private CurrentScene activeScene;
	[System.NonSerializedAttribute]
	//reference to the __app object
	GameObject infoObject;
	
	void Start()
    {
        //seems to be a delay.... might need a wait?
        infoObject = GameObject.Find("__app");
        sceneStateRef = infoObject.GetComponentInChildren<InitScript>();
        activeScene = sceneStateRef.activeScene;
    }
	public void LoadCarSceneByIndexAddative(int sceneIndex){

			//set the current scene... but LoadingScreen 
			activeScene.isLoading = true;
			activeScene.currentIndex = sceneIndex;
			
			LoadingUpdate script = loadingScreen.GetComponent<LoadingUpdate>();
			script.sceneIndex = sceneIndex;
			//activate loadscreen. let it do the loading?
			loadingScreen.SetActive(true);

		}
		
		
	}
