﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class LoadingUpdate : MonoBehaviour {
	public Slider loadingBar;
	public int sceneIndex;
    [System.NonSerializedAttribute]
	private InitScript sceneState;
	[System.NonSerializedAttribute]
	private CurrentScene currentScene;
	[System.NonSerializedAttribute]
	public GameObject _app;
	void Awake(){
		_app = GameObject.Find("__app");
		sceneState = _app.GetComponent<InitScript>();
		currentScene = sceneState.activeScene;
			if(currentScene.isLoading){
				Scene ac = SceneManager.GetActiveScene();
				switch (ac.name)
				{
					case "Mainmenu":
					case "Garage":
						currentScene.loadingProgress.allowSceneActivation = false;
						StartCoroutine(LoadGarageScene(sceneIndex));
						currentScene.loadingProgress.allowSceneActivation = true;
						break;
					default:
						StartCoroutine(LoadingLevel());
						break;
				}
				
			}
	}
	//update slider bar with loading progress
	IEnumerator LoadingLevel ()
    {
		//using the reference to the InitScript CurrentScene object
		//update this GameObject's slider
        while (!currentScene.loadingProgress.isDone)
        {
            loadingBar.value = currentScene.loadingProgress.progress;
            yield return null;
        }
		//deactive the image after 100% load.
		if(currentScene.loadingProgress.isDone){
			currentScene.isLoading = false;
			gameObject.SetActive(false);
			//UPDATE STATE
			sceneState.activeScene = currentScene;
		}
		
    }

	IEnumerator LoadGarageScene(int sceneIndex)
 	{
     Scene activeScene = SceneManager.GetActiveScene(); //does this get garage scene and destroy it? 
	 //(based on the assumstion that garage scene is set to active in the Awake of sectionDetails)
	 
	 //Async object from __app
     currentScene.loadingProgress = SceneManager.LoadSceneAsync(sceneIndex);
	 
	 
     // Wait until the scene has loaded. Once is isDone, it actually shows up at origin,
     while (!currentScene.loadingProgress.isDone){
		 loadingBar.value = currentScene.loadingProgress.progress;
		  yield return null;
	 }
     
     
	 if(currentScene.loadingProgress.isDone){
			currentScene.isLoading = false;
			gameObject.SetActive(false);
			//UPDATE STATE
			sceneState.activeScene = currentScene;
			//Unload old scene
	 		SceneManager.UnloadScene(activeScene);
			StartCoroutine(ClearScene());
		}
     yield return null;
 	}

	 IEnumerator ClearScene(){
		 //unload scene doesn't clear from memory
		yield return new WaitForSeconds(1.0f);
		 Resources.UnloadUnusedAssets();
	 }
}
