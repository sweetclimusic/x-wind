﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class GlobalExit : MonoBehaviour {
    //for going back to sign in menu
    [System.NonSerializedAttribute]
    public InitScript sceneStateRef;
    public int firstScene = 2;
    void Awake() {
        sceneStateRef = Object.FindObjectOfType<InitScript>();
    }
    public void ExitApplication(){
		#if !UNITY_IOS
        Application.Quit();
		#endif
    }
    public void Back() {
        sceneStateRef.activeScene.isLoading = true;
        sceneStateRef.activeScene.loadingProgress = SceneManager.LoadSceneAsync(firstScene);
    }
}
