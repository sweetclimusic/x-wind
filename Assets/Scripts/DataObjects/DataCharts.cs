using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using UnityEngine;

[Serializable]
public class DataPoints {
    public string driverId;
    public string dateTime;
    public string seconds; //because JSON doesn't support floats
    public int distance;
}

[Serializable]
public class DataList{
    public List<DataPoints> dataPoints; ///must have the name of root element in json
}

public static class DataSummaryManager{
    //only has the current drivers data
    public static DataList dataSerialized = new DataList();
    public static bool dataloaded = false;

    /// <summary>
    /// Loading of Datachart data to the static DataSerialized.
    /// </summary>
    public static bool LoadTeamData(){
        string jsonFilePath = Application.persistentDataPath + "/summary.json";
        DataList allDataPoints = new DataList();
        if (!File.Exists(jsonFilePath))
        {
            //create new and end
            File.Create(jsonFilePath);
            return false;
        }
        //load objects into raceTeamList from json.
        string jsonFileAsString = File.ReadAllText(jsonFilePath);
        //send string to utility to populate a object populate object with json data
        JsonUtility.FromJsonOverwrite(jsonFileAsString, allDataPoints);
        //do more on true
        if (allDataPoints.dataPoints != null)
        {
            //find our driver. use linq and find all their datapoints.
            dataSerialized.dataPoints = allDataPoints.dataPoints.Where(s => s.driverId == SelectedTeam.CurrentTeam.driversLiscense ).ToList<DataPoints>();
            dataloaded = (dataSerialized.dataPoints.Count > 0);
        }else
        {
            //depending on a successful load
            dataSerialized.dataPoints = new List<DataPoints>();
        }
        

        return dataloaded;
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param type="DataPoints" name="dataEntry"></param>
    /// <returns>bool true on save</returns>
    public static bool SaveTeamData(DataPoints dataEntry){
        if (dataEntry != null)
        {
           //load existing data or new point of entry
            string jsonFilePath = Application.persistentDataPath + "/summary.json";
            LoadTeamData();
            //file loaded or new file entry
            dataSerialized.dataPoints.Add(dataEntry);

            //var list = allDataPoints.dataPoints.Union(dataSerialized.dataPoints).ToList<DataPoints>();
            //allDataPoints.dataPoints = list;

            //save this to json
            string json = JsonUtility.ToJson(dataSerialized);
           
            if (!File.Exists(jsonFilePath))
            {
                //create new and end
                File.Create(jsonFilePath);
            }
            File.WriteAllText(jsonFilePath, json);
            return true;
        }
        return false;
    }
}