﻿using UnityEngine;
using UnityEngine.UI;


public class RaceTeamButton : MonoBehaviour {

	// Use this for initialization
	public Button raceTeamButton;
	public Image teamIcon;
	public Text teamName;
	public Text mainDriver;
	public Text secondDriver;
	private string licenseId;
	public Button removeRaceTeam;
	public string LicenseId {
					get{return licenseId;}
					set{licenseId = value;}
					}
	private Teams currentTeamObject;
	public Teams CurrentTeam{
		get{return currentTeamObject;}
		set{currentTeamObject = value;}
	}
}
