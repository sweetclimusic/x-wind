﻿using UnityEngine;
using System.Collections.Generic;
//Class to define my users Json object to use the UnityEngine JsonUtility.FromJson
//http://stackoverflow.com/questions/38800533/unity-error-parsing-json-from-remote-source
[System.Serializable]
public class  Teams{
		public string teamName;
		public string driverName1;
		public string driverName2;
		public string driversLiscense; //System.GUID.newGUID()
		
		public string color;
		public int spindalPosition;
    	public int rearAxelPosition;
    	public int wheelType;
		public int bandType;
		public bool posOne;
		public bool posTwo;
		public bool posThree;
		public bool posFour;
		public bool posFive;

	}
[System.Serializable]
public class TeamsList{

	public List<Teams> teams;
}