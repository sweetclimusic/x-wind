﻿
Shader "GTS/Outlined/Outline Diffuse" {
    Properties{
        _Color("Main Color", Color) = (.5,.5,.5,1)
        _OutlineColor("Outline Color", Color) = (0,0,0,1)
        _Outline("Outline width", Range(0.0, 3.0)) = .005
        _MainTex("Base (RGB)", 2D) = "white" { }
    }

    CGINCLUDE
    #include "UnityCG.cginc"

    struct appdata 
    {
        float4 vertex : POSITION;
        float3 normal : NORMAL;
    };

    struct v2f 
    {
        float4 pos : POSITION;
        float4 color : COLOR;
        float3 normal : NORMAL;
    };

    uniform float _Outline;
    uniform float4 _OutlineColor;

    v2f vert(appdata v) 
    {
        //v.normal.xy *= _Outline;
        v.vertex.xyz *= _Outline;

        v2f o;

        o.pos = UnityObjectToClipPos(v.vertex);
        o.normal = UnityObjectToClipPos(v.normal);
        o.color = _OutlineColor;
        return o;
    }

    ENDCG

    SubShader
    {
        //Change Queue to AlphaTest for to merger two outlines of differnet objects
        //Tags{ "Queue" = "AlphaTest" }

        // Make the outline
        Pass
        {
            Name "OUTLINE"
            Tags{ "LightMode" = "Always" }
            ZWrite Off

            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            half4 frag(v2f i) :COLOR
            {
                return i.color;
            }
            ENDCG
        }


        Tags{ "Queue" = "Transparent" }
        // Draw the object normally
        Pass
        {
            Name "BASE"
            ZWrite On

            Material
            {
                Diffuse[_Color]
                Ambient[_Color]
            }

            Lighting On

            SetTexture[_MainTex]
            {
                ConstantColor[_Color]
            }

            SetTexture[_MainTex]{
                Combine previous * primary DOUBLE
            }
        }
    }

    Fallback "Diffuse"
}

